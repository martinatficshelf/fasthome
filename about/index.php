<?php
    require_once("../php/elements.php");
?>
        <?php
            printHeader(true);
        ?>
        
        <section>
            <h1>
                About us
            </h1>
            <h2>
               A place for readers to discover, and for writers and publishing professionals to make a living from their craft. 
            </h2>
            <p>
                Welcome to FicShelf Beta! We have a simple but ambitious vision: to professionalise the world of self-publishing and to support writers, editors, proof readers and designers to make a living from their talent. We aim to address the many practical problems facing our industry today. The intelligent way to do this is to develop bit by bit, so we’re launching in three phases, beginning with the FicShelf Marketplace.
            </p>
        </section>
        <section>
            <h3>
                Phase 1: The Marketplace
            </h3>
            <p>
               Through our Marketplace writers and publishing professionals can connect to form their very own publishing teams. We don’t want funding to be a barrier to great writing, so we’re including the opportunity for writers to fundraise via our platform too. And we’re providing all the tools needed to develop high quality eBooks. 
            </p>
            <h3>
                Phase 2: Social Publishing
            </h3>
            <p>
                In the 19th century, Charles Dickens made a living by serialising many of his greatest works through journals and periodicals, and this provided us with the inspiration for the second phase of our journey. Our social publishing platform allows writers to engage readers in the creative process, and readers to reward their favourite writers, as they write, chapter by chapter. This adds another dimension to the creative process, making reading and writing a more reciprocal experience.
            </p>
            <h3>
                Phase 3: The eBookshop
            </h3>
            <p>
                We want to make eBook buying and selling a more intimate and ethical process, so we’re helping each of our authors to create their very own eBookshop. This enables transactions to take place directly between the writer and their readers. Readers know that their money is going straight into supporting the writers they love, and writers are able to build a relationship with their fans.
            </p>
        </section>
        <section>
            <p class="red">We’ve spent many years of research, and many months of development to get FicShelf Beta ready for you, and we welcome and encourage you to provide feedback. We want to grow into a robust platform, but the only way for us to do this is by having you – our users – directly involved. Your opinion is more important than ours, so please don’t hold back.</p>
            <p class="red">Join us and help us change the face of publishing.</p>
            <div id="btn_container" class="center">
                <a href="http://beta.ficshelf.com" class="btn">JOIN IN</a>
            </div>
        </section>
    </div>

    <?php
        printFooter(true);
    ?>
  </body>
</html>

