<?php
    require_once("../php/elements.php");
?>

    <?php
       printHeader(false);
    ?>

      <section>
        <h1>FicShelf Limited - Terms &amp; Conditions</h1>
        <h2>
          We are FicShelf Limited (“FicShelf”) incorporated in the UK under company number 08342649. These are our terms and conditions. They tell you what we do and how we do it.
        </h2>
        <p>
          By using our site, you indicate that you accept these terms and conditions, and the referenced policies, and that you agree to abide by them.
        </p>
        <p>
          These terms and conditions are effective from 1st May 2014.
        </p>
      </section>
      
      <section>
        <h3>1. The Terms</h3>
        <ul>
          <li>
            Services are offered on FicShelf through a fixed price per job to be agreed by the parties.
          </li>
          <li>
            On Commencement of a Job the Buyer pays 30% of the agreed amount.The remaining 70% is due upon completion of the Job.
          </li>
          <li>
            The Service Provider must provide regular progress updates and respond within two (2) working days to all messages from the Buyer via the Platform.
          </li>
          <li>
            Service Providers must deliver work within the delivery times as agreed with the Buyer upfront. Service Providers' rankings may be penalised for late delivery.
          </li>
          <li>
            Service Providers must fulfill their Jobs and Buyers agree to pay for work delivered. Users will be penalised for cancellations or refunds caused by them, without just reason.
          </li>
          <li>
            Service Providers must provide, and be given the opportunity by the Buyer to provide, at least two further iterations on the work delivered if the Buyer is not initially satisfied.
          </li>
          <li>
            Once the Job has been completed the Service Provider will raise a request to receive payment. Service Providers must not request payment upfront before work has been completed. Buyers must pay within the 7 day payment terms.
          </li>
          <li>
            All payments for work completed must go via FicShelf; attempts to pay outside of FicShelf will lead to sanctions not limited to immediate account suspension. Users must immediately report to FicShelf any offers to pay outside of FicShelf made by their Buyer or Service Provider.
          </li>
          <li>
            Once payment has been made, Buyers are granted all rights for the delivered work.
          </li>
          <li>
            User will receive service messages, notices, offers and news about FicShelf. Users may receive alerts on certain pages and emails to the email addresses associated with their accounts. Users can easily unsubscribe from any email notifications using the link found at the end of any email as well as on their Account Settings page.
          </li>
        </ul>
      </section>

      <section>
        <h3>2. User accounts</h3>
        <ul>
          <li>
            In order to use FicShelf, Users have to register for an account, provide accurate, legal and complete information, and keep their account information updated. Both Buyers and Service Providers undergo the same account registration process.
          </li>
          <li>
            Each account must be a personal account, but Users may trade as a sole trader, company or any other legal entity (whether incorporated or unincorporated).
          </li>
          <li>
            Users cannot register for more than one account.
          </li>
          <li>
            FicShelf reserve the right to restrict your access temporarily, indefinitely block your account, stop any Jobs you have in progress, warn other Buyers and Service Providers of your actions or issue you a warning if:
            <br>
            a) you breach the letter or spirit of these terms and conditions or the referenced policies;
            <br>
            b) we are unable to verify or authenticate any information you provide to us;
            <br>
            c) we believe that your actions may cause any loss or liability to our Users or to us
            <br>
            d) you use your account for any illegal activities.
          </li>
          <li>
            Users are solely responsible for any activity that occurs on their account, unless it is a result of actions beyond their control (such as hacking or if someone steals their password when they have taken reasonable steps to keep it secure). Users may never use another person's user account or registration information for the Website.
          </li>
        </ul>
      </section>

      <section>
        <h3>3. Job delivery</h3>
        <ul>
          <li>
            Before commencement, the Buyer and the Service Provider need to agree on the details of the job.
          </li>
          <li>
            Once the requirements are agreed, the Buyer needs to pay 30% of the agreed fixed price so that the Service Provider can get started.
          </li>
          <li>
            Once the requirements are provided the Service Provider must:
            <br>
            a) Confirm that work has started otherwise the Buyer may request and will be entitled to a refund;
            <br>
            b) provide regular progress updates and respond within two (2) working days to all messages from the Buyer;
            <br>
            c) complete the Job within the delivery timescales indicated in the Job description including handover of all deliverables. Failing to deliver within those timescales may mean the Buyer requests and is entitled to a refund in accordance with Section 6;
            <br>
            d) Late or non-delivery will negatively affect the Service Provider's rankings on FicShelf.
            <br>
            All direct communication between Buyer and Service Provider must go via the FicShelf Platform messaging system.
          </li>
          <li>
            Once the Job has been completed to the Buyer’s satisfaction, the Buyer must pay the remaining 70% of the agreed fixed price and:
            <br>
            a) the Buyer is then able to leave feedback for the Service Provider;
            <br>
            b) the fees charged in the Invoice should be as has been explicitly agreed upfront in the Proposal. If requirements/ scope change materially during the Job the Service Provider may need to propose an amendment to a previously agreed price. This must be explicitly agreed before any extra work is undertaken so that the Buyer can confirm whether they wish to proceed with the additional scope for the extra cost;
            <br>
            c) if the Buyer is not satisfied with the deliverables they can reject the invoice and provide the Service Provider with detailed feedback on what work remains outstanding. The Service Provider must provide, and be given the chance to provide, at least two revisions of the deliverables based on detailed feedback from the Buyer;
            <br>
            d) the Buyer must pay the remaining 70% within seven (7) days of the Job’s completion;
            <br>
            e) any disputes between the Buyer and the Service Provider must be resolved between the parties.
          </li>
        </ul>
      </section>

      <section>
        <h4>3.1 Quality</h4>
        <p>
          All Service Providers on FicShelf must strive to deliver a high standard of work, appropriately meeting their Buyer's needs.
        </p>
        <p>
          Specifically, Service Providers must ensure that all deliverables provided in a Job:
        </p>
        <ol type="a">
          <li>
            are error free;
          </li>
          <li>
            fully address each of the Buyer's defined requirements; and
          </li>
          <li>
            are of a standard consistent with the level of expertise indicated in the Service Provider's profile.
          </li>
        </ol>
        <h4>3.2 The heavy but important bits:</h4>
        <ul>
          <li>
            All Jobs sourced through the FicShelf platform must remain within the platform until completion.
          </li>
          <li>
            The Buyer and Service Provider create a direct service contract between themselves and FicShelf are not party to that contract.
          </li>
          <li>
            Ownership in and to any materials and or deliverables arising from a Job and any intellectual property rights therein, will be assigned to the Buyer upon successful payment for the Job. If the Buyer and Service Provider wish to include any special terms of the contract (for example, in relation to the ownership of the work produced, ownership of intellectual property rights or special rights of termination), the Buyer and Service Provider should negotiate and document these terms.
          </li>
          <li>
            The terms and conditions of each Job shall be deemed to incorporate a term that in consideration of the fee paid for the Job the Service Provider thereby assigns to the Buyer absolutely with full title guarantee the following rights throughout the world:
            <br>
            a) the entire copyright and all other rights in the nature of copyright subsisting in the Job;
            <br>
            b) any database right subsisting in the Job; and
            <br>
            c) all other rights in the Job of whatever nature, whether now known or created in the future, to which the Service Provider is now, or at any time after the date of Commencement of the Job may be, entitled by virtue of the laws in force in the United Kingdom and in any other part of the world,
          </li>
          <li>
            in each case for the whole term including any renewals, reversions, revivals and extensions and together with all related rights and powers arising or accrued, including the right to bring, make, oppose, defend, appeal and obtain relief (and to retain any damages recovered) in respect of any infringement, or any other cause of action arising from ownership, of any of these assigned rights, whether occurring before, on, or after the date of Commencement of the Job.
          </li>
          <li>
            You promise that:
            <br>
            a) you are able to pay for the services if you are a Buyer and perform the services if you are a Service Provider;
            <br>
            b) you are not in breach of any applicable laws, rules or regulations or obligations to any other person;
            <br>
            c) you have made and will make all required legal and tax filings. If relevant, you will file all necessary legal documentation relating to your self-employment required by any governmental body, and pay all applicable taxes including without limitation PAYE or other income tax and national insurance;
            <br>
            d) you shall not (and shall not permit) any third party to either take any action or upload, download, post, submit or otherwise distribute or facilitate distribution of any Content, Job, or Proposal on or through the services FicShelf provide, including without limitation any User Content, that infringes any patent, trademark, trade secret, copyright or other right of any other person or entity or violates any law or contractual duty.
          </li>
        </ul>
      </section>

      <section>
        <h3>4. Payments</h3>
        <ul>
          <li>
            Payments are made as described in Section 3, for a Job agreed by the Buyer as successfully completed. The payment terms are seven (7) days.
          </li>
          <li>
            The Service Provider must not request payment prior to Job completion; Buyers must report such cases immediately. In such cases the Service Provider's account may be temporarily or permanently suspended..
          </li>
          <li>
            All payments between Buyer and Service Provider must be processed through FicShelf as described in Section 3, both for work sourced on FicShelf and for any follow on work between the Buyer and Service Provider, either for the same Job or another Job..
          </li>
          <li>
            Payment (or attempt of) outside of FicShelf is a breach of these terms and conditions and will lead to temporary and/ or permanent suspension of the Buyer and/ or Service Provider's account. The Service Provider will be liable for any loss of business and legal expenses that FicShelf may incur in recovering it. FicShelf reserve the right to sanction funds held in the Service Provider's account in order to recover lost fees. Moreover, FicShelf will not mediate any Disputes or be liable to either the Buyer or Service Provider's loss of business as a result of violation of this clause. Users must report attempts or offers to make payment outside of FicShelf by their Buyer or Service Provider to FicShelf immediately..
          </li>
          <li>
            Payments by Buyers are routinely checked by FicShelf for fraud prevention purposes, before any payments are released to the Service Provider. FicShelf aim to process these payments and credit funds to the Service Provider's FicShelf Wallet within three (3) working days, however FicShelf reserve the right to take up to ten (10) working days..
          </li>
          <li>
            To withdraw funds from their FicShelf Wallet a Service Provider needs to request a withdrawal from their dashboard. FicShelf process payments daily and will send the money to the Service Provider via Paypal. Once FicShelf has processed payments, they are then subject to timescales imposed by Paypal..
          </li>
          <li>
            For security reasons, FicShelf reserves the right to request additional information from Buyers and Service Providers, including original documents, and to verify documents with issuing institutions. Therefore, we reserve the right to request the following proofs of identity:
            <br>
            - a copy of a Government issued ID (Passport, Driver's License or National ID Card);
            <br>
            - a copy of a recent utility bill showing your name and address (less than 3 months old);
            <br>
          </li>
          <li>
            For PayPal users:
            <br>
            - PayPal Account statement showing your PayPal registered name, email address and verification status along with any relevant transactions.
          </li>
          <li>
            FicShelf is not a deposit taking body and any monies held by FicShelf on a User's behalf are not insured deposits. Users will not receive interest or other earnings on the funds in their FicShelf Account. FicShelf will endeavour to ensure that the funds in the FicShelf Wallet are available to the User in accordance with these Terms and Conditions but do not guarantee that they will be available to the User in circumstances which are unforeseen or beyond our control.
          </li>
        </ul>

        <p>
          <strong>PayPal is a registered trademark of PayPal, Inc</strong>
        </p>
      </section>

      <section>
        <h3>5. Leaving Feedback</h3>
        <ul>
          <li>
            When the Job is completed and the Service Provider has been paid by the Buyer, both parties are asked to provide both qualitative feedback and a rating from 1-5 for the other party. This rating influences each User's ranking on FicShelf.
          </li>
          <li>
            Both parties should complete the feedback honestly. Users must not falsify feedback, manipulate or coerce another User by threatening negative feedback or offering incentives in exchange for feedback. Any attempts of this nature should be reported immediately to FicShelf.
          </li>
          <li>
            Feedback comments that are reported to us as defamatory, abusive or offensive will be reviewed and may be removed at our discretion.
          </li>
        </ul>
        <h3>6. FicShelf fees</h3>
        <h4>6.1 Buyer Fees</h4>
        <p>
          FicShelf do not charge a fee to the Buyer. FicShelf only pass on  Paypal’s 1.9% payment processing fees on deposit of funds.
        </p>
        <h4>6.2 Service Provider fees</h4>
        <ul>
          <li>
            FicShelf charge Service Providers a Service Fee of a 10% (excl. VAT) per transaction.
          </li>
          <li>
            The minimum Service Fee per invoice is £1 (excl. VAT).
          </li>
          <li>
            All Users are provided with a statement at the end of each month showing their net effective commission fee in their ‘Payments' section on the Website.
          </li>
          <li>
            When a Service Provider withdraws funds from their FicShelf Wallet, FicShelf pass on the paypal standard withdrawal fee of 1.9%. This fee is stipulated by paypal and it may change at their discretion.
          </li>
        </ul>
        <h4>6.3 Admin fee for inactive User accounts</h4>
        <p>
          If a User has funds in their FicShelf Wallet and the User has not logged in to their FicShelf account for a period of six (6) months or more then FicShelf will charge a monthly administration fee of £3.50 (excl. VAT) for holding those funds on behalf of the inactive User. This fee will be automatically collected each month from the funds in the User's FicShelf Wallet starting from the seventh (7th) calendar month since the User last logged in to their User account. The collection of this fee will end when either i) the account becomes active again after the User logs back in to FicShelf, or ii) the balance in the FicShelf Wallet is cleared.
        </p>
      </section>

      <section>
        <h3>7. Privacy Statement</h3>
        <p>
          By proceeding to use the FicShelf service Users consent that FicShelf may process the personal data (including sensitive personal data) that FicShelf collects from them in accordance with the FicShelf Privacy Policy.
        </p>

        <h3>8. Governing Law</h3>
        <p>
          These terms and conditions and any dispute or claim arising out of or in connection with them or their subject matter or formation (including non-contractual disputes or claims) shall be governed by and construed in accordance with the law of England and Wales. The courts of England and Wales shall have non-exclusive jurisdiction to settle any dispute or claim that arises out of or in connection with these terms and conditions or their subject matter or formation (including non-contractual disputes or claims).
        </p>
        <h3>9. Definitions</h3>
        <p>
          In these terms and conditions:
        </p>
        <p>
          <strong>'Buyer'</strong> means a person buying services from a Service Provider on FicShelf;
        </p>
        <p>
          <strong>'Commencement of the Job'</strong> means the Buyer assigning a Service Provider to a Job;
        </p>
        <p>
          <strong>‘Content'</strong> means such things as data, text, photographs, videos, audio clips, written posts and comments, graphics, User content and interactive features generated, provided, or otherwise made accessible on or through FicShelf;
        </p>
        <p>
          <strong>'Custom Job'</strong> is a piece of work that commences with the acceptance of a Proposal by a Buyer. This work is agreed either after the Buyer has posted a request on FicShelf or in direct communication between a Buyer and a Service Provider;
        </p>
        <p>
          <strong>‘Dispute'</strong> means a dispute raised in relation to a rejected Invoice or a rejected Refund Request;
        </p>
        <p>
          <strong>'Invoice'</strong> means a bill for a completed Job which is raised by the Service Provider in the WorkStream or in the FicShelf payments dashboard;
        </p>
        <p>
          <strong>'Job'</strong> means a piece of work that a Service Provider and Buyer agree via FicShelf is to be provided by the Service Provider to the Buyer. Job refers to work either agreed when the Buyer purchases an Hourlie™, or when a Proposal by a Service Provider is accepted by the Buyer for a Custom Job;.
        </p>
        <p>
          <strong>‘FicShelf'</strong> means the website with the domain name ficshelf.com or FicShelf Limited as the context so requires;
        </p>
        <p>
          <strong>‘FicShelf Wallet'</strong> means the online electronic Wallet which holds your money that is available to you to withdraw at anytime;
        </p>
        <p>
          <strong>'Proposal'</strong> means an offer made by a Service Provider to a Buyer to provide a Custom Job and which must contain a fee quotation;
        </p>
        <p>
          <strong>‘Service Provider'</strong> means a person selling services to a Buyer via FicShelf;
        </p>
        <p>
          <strong>‘User'</strong> means any person who uses FicShelf;
        </p>
        <p>
          <strong>‘User Content'</strong> means all Content uploaded, submitted, distributed, or posted to the services by Users, including without limitation, Proposals and communication via FicShelf. User Content does not include any materials or deliverables, or intellectual property therein arising from a Job, which shall be assigned to the Buyer on successful payment for the Job. User Content is the sole responsibility of the person who originated it.
        </p>
      </section>
    </div>

    <?php
        printFooter(false);
    ?>

  </body>
</html>