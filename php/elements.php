
<?php
  function printHeader($isAbout) {
    $pr = $isAbout ? '' : '../about/'
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FicShelf - About us</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $pr; ?>css/normalize.css">
    <link rel="stylesheet" href="<?php echo $pr; ?>css/foundation.min.css">
    <link rel="stylesheet" href="<?php echo $pr; ?>css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="../home/js/foundation.min.js"></script>
  </head>
  <body>
    <div class="off-canvas-wrap" data-offcanvas>
      <div class="inner-wrap">

        <!-- Off Canvas Menu -->
        <aside class="right-off-canvas-menu">
            <ul class="off-canvas-list">
              <li><a href="..">HOME</a></li>
              <li><a href="/about">ABOUT US</a></li>
              <li><a href="http://beta.ficshelf.com"><i class="fa fa-lock"></i>LOG IN</a></li>
            </ul>
        </aside>

        <div id="content">
        <header>
            <div class="row">
                <div class="small-10 medium-4 large-6 column">
                    <a href="http://ficshelf.com">
                        <img src="<?php echo $pr; ?>images/FS-logo.svg" alt="" />
                    </a>
                </div>

                <div class="small-2 medium-8 large-6 column">
                    <div id="menu" class="right hide-for-small-only right">
                        <a href="..">HOME</a>
                        <?php
                            if ($isAbout)
                                echo "<a class=\"selected\">ABOUT US</a>";
                            else
                                echo "<a href=\"../about\">ABOUT US</a>";
                        ?>
                        <a href="http://beta.ficshelf.com"><i class="fa fa-lock"></i>LOG IN</a>
                    </div>

                 <!-- mobile menu toggle button -->
                  <a class="right-off-canvas-toggle show-for-small-only" href="#"><img id="mobile_munu_icon"  src="<?php echo $pr; ?>images/menu-icon.svg" alt="" /></a>
                </div>    
            </div>
        </header>
<?php    
  }
?>


<?php
  function printFooter($isAbout) {
?>
    <footer>
        <div class="row">
            <div class="medium-4 column">
<a class="about_us_link <?php echo $isAbout ? 'selected' : '' ?>" <?php echo !$isAbout ? 'href="/about"' : '' ?>>About Us</a>&nbsp;<a  class="<?php echo !$isAbout ? 'selected' : '' ?>" <?php echo $isAbout ? 'href="/terms-and-conditions"' : '' ?>>Terms &amp; Conditions</a>
            </div>
            <div class="medium-4 column">
                &copy;&nbsp;2014&nbsp;FicShelf Limited
            </div>
            <div class="medium-4 column socials">
                <a href="https://www.facebook.com/Ficshelf">
                </a>
                <a href="https://twitter.com/ficshelf">
                </a>
            </div>
        </div>
    </footer>

        <a class="exit-off-canvas show-only"></a>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready(function() {
         $(document).foundation();
      });
    </script>
<?php    
  }
?>