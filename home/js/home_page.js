var ActionChanger = {
  BACK_DELAY: 1500,
  strings: ["Write.", "Read.", "Edit.", "Design."],
  colors:  ['#f15746', '#448687', '#9c0f17', '#55a9aa'],
  curIdx: -1,

  preStringTyped: function() {
    this.curIdx = (this.curIdx + 1) % this.strings.length;
    var span = $("#caption span"); // #type & cursor
    span.css('color', this.colors[this.curIdx]);

    // allows to align image horizontaly
    span = span.eq(0);
    span.css('visibility', 'hidden');
    span.text(this.strings[this.curIdx]);
    span.parent().css('width', span.width() + 'px');
    span.text('');
    span.css('visibility', 'visible');

    var img = $('#animation img').eq(this.curIdx);
    img.delay(100).fadeIn('slow');

  },
  onStringTyped: function() {
    setTimeout(this._hideImage, this.BACK_DELAY);
  },
  _hideImage: function() {
    var curImg = $('#animation img:visible');
    curImg.delay(100).fadeOut('slow');
  },
  
  // hack required after off-canvas menu open (in Chrome)
  onOpenMobileMenu: function() {
    var tObj = $("#type").data("typed")
    tObj.typeSpeed = 9999999999;
    tObj.backSpeed = 9999999999;
    $("#type").text('');
    $('#animation img').hide();
  },
  onHideMobileMenu: function() {
    var tObj = $("#type").data("typed");
    var string = this.strings[0];
    this.curIdx = -1;
    setTimeout(function() {
        tObj.typewrite(string, 0);
      }, 550
    )
    $("#type").text('');
    $('#animation img').hide();
    tObj.typeSpeed = 150;
    tObj.backSpeed = 50;
  }
}

/* on ready document */
$(document).ready(function() {
     $(document).foundation();
     
     $('body').on('click', 'a.scroll_btn', function(){  
      $('html, body').animate({
          scrollTop: $( $(this).attr('href') ).offset().top
      }, 500);
      return false;
    });

    $("#type").typed({
      strings: ["Write.", "Read.", "Edit.", "Design."],
      startDelay: 100,
      typeSpeed: 150,
      backSpeed: 50,
      backDelay: ActionChanger.BACK_DELAY,
      loop: true,
      preStringTyped: ActionChanger.preStringTyped.bind(ActionChanger),
      onStringTyped: ActionChanger.onStringTyped.bind(ActionChanger)
    });

    // hack required after off-canvas menu open (in Chrome)
    $(".right-off-canvas-toggle").on("click", ActionChanger.onOpenMobileMenu);
    $(".exit-off-canvas").on("click", ActionChanger.onHideMobileMenu.bind(ActionChanger));

});
